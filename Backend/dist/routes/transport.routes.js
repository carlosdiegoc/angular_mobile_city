"use strict";
exports.__esModule = true;
exports.transportRoutes = void 0;
var express_1 = require("express");
var CreateTransportController_1 = require("../modules/transports/useCases/createTransport/CreateTransportController");
var createTransportController = new CreateTransportController_1.CreateTransportController();
var transportRoutes = (0, express_1.Router)();
exports.transportRoutes = transportRoutes;
transportRoutes.post("/", createTransportController.handle);
//# sourceMappingURL=transport.routes.js.map