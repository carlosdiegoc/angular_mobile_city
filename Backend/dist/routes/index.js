"use strict";
exports.__esModule = true;
exports.routes = void 0;
var express_1 = require("express");
var client_routes_1 = require("./client.routes");
var transport_routes_1 = require("./transport.routes");
var routes = (0, express_1.Router)();
exports.routes = routes;
routes.use("/clients", client_routes_1.clientRoutes);
routes.use("/transports", transport_routes_1.transportRoutes);
//# sourceMappingURL=index.js.map