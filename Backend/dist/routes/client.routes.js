"use strict";
exports.__esModule = true;
exports.clientRoutes = void 0;
var express_1 = require("express");
var CreateClientController_1 = require("../modules/clients/useCases/createClient/CreateClientController");
var createClientController = new CreateClientController_1.CreateClientController();
var clientRoutes = (0, express_1.Router)();
exports.clientRoutes = clientRoutes;
clientRoutes.post("/", createClientController.handle);
//# sourceMappingURL=client.routes.js.map